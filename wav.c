#include<stdio.h>
#include<stdlib.h>
#include<stdint.h>
#include<string.h>

int main(int argc, char *argv[])
{
    FILE *image;
    FILE *wave;
    uint16_t type[2];
    uint32_t data;
    int32_t width;
    int32_t height;
    uint16_t bits;
    uint32_t compression;
    int row,i,k,b,g,r,pixel,minpixel,minh;
    float sample;
    uint32_t Subchunk1Size=16;
    uint16_t AudioFormat=3;
    uint16_t NumChannels=1;
    uint32_t SampleRate=48000;
    uint16_t BitsPerSample=32;
    uint16_t BlockAlign=NumChannels*BitsPerSample/8;
    uint32_t ByteRate=SampleRate*BlockAlign;
    uint32_t Subchunk2Size=width*BlockAlign;
    uint32_t ChunkSize=36+Subchunk2Size;

    //Image metadata parsing and error checking.
    if(argc<3)
    {
        fprintf(stderr,"Not enough parameters provided\n");
        exit(1);
    }
    image=fopen(argv[1],"rb");
    if(image==NULL)
    {
        fprintf(stderr,"Cannot open file: %s\n",argv[1]);
        exit(1);
    }
    fread(&type,2,1,image);
    if(memcmp(&type,"BM",2)!=0)
    {
        fprintf(stderr,"Unknown BMP type: %s\n",type);
        exit(1);
    }
    fseek(image,10,SEEK_SET);
    fread(&data,4,1,image);
    fseek(image,18,SEEK_SET);
    fread(&width,4,1,image);
    row=width*3;
    if(row%4!=0)
    row=row+4-row%4;
    fread(&height,4,1,image);
    fseek(image,28,SEEK_SET);
    fread(&bits,2,1,image);
    if(bits!=24)
    {
        fprintf(stderr,"Bit depth should be 24 (bgr24 format). Currently: %i\n",bits);
        exit(1);
    }
    fread(&compression,4,1,image);
    if(compression!=0)
    {
        fprintf(stderr,"This program supports only uncompressed BMP images\n");
        exit(1);
    }

    //Writing WAVE header
    wave=fopen(argv[2],"wbx");
    if(wave==NULL)
    {
        fprintf(stderr,"Cannot create file: %s\n",argv[2]);
        exit(1);
    }
    fwrite("RIFF",4,1,wave);
    fwrite(&ChunkSize,4,1,wave);
    fwrite("WAVE",4,1,wave);
    fwrite("fmt ",4,1,wave);
    fwrite(&Subchunk1Size,4,1,wave);
    fwrite(&AudioFormat,2,1,wave);
    fwrite(&NumChannels,2,1,wave);
    fwrite(&SampleRate,4,1,wave);
    fwrite(&ByteRate,4,1,wave);
    fwrite(&BlockAlign,2,1,wave);
    fwrite(&BitsPerSample,2,1,wave);
    fwrite("data",4,1,wave);
    fwrite(&Subchunk2Size,4,1,wave);

    //Parsing pixel data and writing audio samples.
    for(i=0;i<width;i++)
    {
        minpixel=3*255;
        minh=0;
        for(k=0;k<height;k++)
        {
            fseek(image,data+k*row+i*3,SEEK_SET);
            b=0;
            g=0;
            r=0;
            fread(&b,1,1,image);
            fread(&g,1,1,image);
            fread(&r,1,1,image);
            pixel=b+g+r;
            if(pixel<minpixel)
            {
                minpixel=pixel;
                minh=k;
            }
        }
        sample=(float)2*minh/(height-1)-1;
        fwrite(&sample,4,1,wave);
    }

    //Closing up
    fclose(image);
    fclose(wave);
    return 0;
}
